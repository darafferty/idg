import os
import ctypes
import numpy.ctypeslib
from Proxy import *

class CPU(Proxy):
    lib = idg.load_library('libidg-cpu.so')

class Reference(CPU):

    def __init__(
        self,
        nr_correlations,
        subgrid_size):
        """Reference CPU implementation"""
        print self.lib
        try:
            self.lib.CPU_Reference_init.argtypes = [ctypes.c_uint, \
                                               ctypes.c_uint]
            self.obj = self.lib.CPU_Reference_init(
                ctypes.c_uint(nr_correlations),
                ctypes.c_uint(subgrid_size))
        except AttributeError:
            print "The chosen proxy was not built into the library"

class Optimized(CPU):

    def __init__(
        self,
        nr_correlations,
        subgrid_size):
        """Optimized CPU implementation"""
        try:
            self.lib.CPU_Optimized_init.argtypes = [ctypes.c_uint, \
                                               ctypes.c_uint]
            self.obj = self.lib.CPU_Optimized_init(
                ctypes.c_uint(nr_correlations),
                ctypes.c_uint(subgrid_size))
        except AttributeError:
            print "The chosen proxy was not built into the library"

